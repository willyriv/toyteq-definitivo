<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! $messages ){
	return;
}

?>

<?php foreach ( $messages as $message ) : ?>
	<div class="woocommerce-message grve-woo-message grve-bg-green" role="alert">
		<?php
			if ( function_exists( 'wc_kses_notice' ) ) {
				echo wc_kses_notice( $message );
			} else {
				echo wp_kses_post( $message );
			}
		?>	
	</div>
<?php endforeach;
	
//Omit closing PHP tag to avoid accidental whitespace output errors.
