<?php
/**
 * Single Product Rating
 *
 * @see     http://yourhotcompany.su/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( function_exists( 'wc_review_ratings_enabled' ) ) {
	if ( ! wc_review_ratings_enabled() ) {
		return;
	}
} else {
	if ( 'no' === get_option( 'woocommerce_enable_review_rating' ) ) {
		return;
	}
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

if ( $rating_count > 0 ) : ?>

<?php if ( version_compare( WC_VERSION, '3.0', '<' ) ) { ?>
	<div class="woocommerce-product-rating">
		<div class="star-rating">
			<span style="width:<?php echo ( ( $average / 5 ) * 100 ); ?>%">
				<?php
				/* translators: 1: average rating 2: max rating (i.e. 5) */
				printf(
					__( '%1$s out of %2$s', 'woocommerce' ),
					'<strong class="rating">' . esc_html( $average ) . '</strong>',
					'<span>5</span>'
				);
				?>
				<?php
				/* translators: %s: rating count */
				printf(
					_n( 'based on %s customer rating', 'based on %s customer ratings', $rating_count, 'woocommerce' ),
					'<span class="rating">' . esc_html( $rating_count ) . '</span>'
				);
				?>
			</span>
		</div>
	</div>
<?php } else { ?>
	<div class="woocommerce-product-rating">
		<?php echo wc_get_rating_html( $average, $rating_count ); ?>
	</div>
<?php } ?>	

<?php endif; ?>